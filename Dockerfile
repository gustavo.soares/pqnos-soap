FROM php:7.0-fpm
RUN apt update && apt upgrade -y
RUN apt install libxml2-dev -y
RUN docker-php-ext-install xml
RUN docker-php-ext-install json
RUN docker-php-ext-install soap
