<?php

try {

	//define('WP_USE_THEMES', false);
	//require_once('../../../../wp-load.php');
	require	'bhw.php';

	$PegaUltimosEstoquesProdutos = $soap->get(array(
		'Dias' => 3,
		'CDFilial' => '1'
	),'PegaUltimosEstoquesProdutos');

	$v = (object) $PegaUltimosEstoquesProdutos;
	$r = new ReflectionObject($v);

	$PegaUltimosEstoquesProdutos = implode(', ', array_map(
		 function($p) use ($v) {
			 $p->setAccessible(true);
			 return $p->getName() .': '. $p->getValue($v);
		 }, $r->getProperties())) ;

	$PegaUltimosEstoquesProdutos = str_replace('PegaUltimosEstoquesProdutosResult: ', '', $PegaUltimosEstoquesProdutos);
	$PegaUltimosEstoquesProdutos = str_replace('utf-16', 'utf-8', $PegaUltimosEstoquesProdutos);

	$xmlPegaUltimosEstoquesProdutos = simplexml_load_string($PegaUltimosEstoquesProdutos);

	$texto = "";

	foreach ($xmlPegaUltimosEstoquesProdutos->EstoqueProduto as $EstoqueProduto) {

		//$postid = wc_get_product_id_by_sku( (string) $EstoqueProduto->CDProduto);
		$postid = 1;

		if( !empty( $postid ) ) {

			//$_product = wc_get_product( $postid );

			$texto .= "\r\n- INICIANDO A ATUALIZACAO DO PRODUTO -";
			$texto .= "\r\ncodigo: " . $EstoqueProduto->CDProduto;
			$texto .= "\r\n";
			$texto .= "estoque: " . $EstoqueProduto->Estoque;
			$texto .= "\r\n";

			//$_product->set_manage_stock( 'yes' );
			//$_product->save();

			if( (int) $EstoqueProduto->Estoque > 0 ) {
				//wc_update_product_stock_status( $postid, 'instock' );
			} else {
				//wc_update_product_stock_status( $postid, 'outofstock' );
			}

			//wc_update_product_stock( $postid, (int) $EstoqueProduto->Estoque );

			//do_action( 'woocommerce_product_quick_edit_save', $_product );

			$texto .= "- FINALIZANDO A ATUALIZACAO DO PRODUTO -\r\n";

		}

	}

	date_default_timezone_set('America/Sao_Paulo');

	$date = date('d/m/Y H:i:s');
	$date_arquivo = date('d-m-Y');
	$texto .= "\r\nExecutado em: " . $date;
	$texto .= "\r\n\r\n----------------------------------------------------\r\n";

	$arquivo = fopen("logs/logs_estoque_" . $date_arquivo . ".txt", "a") or die("Impossibilitado de abrir o arquivo!");
	fwrite($arquivo, "\r\n". $texto);
	fclose($arquivo);

	echo '[' . $date . '] Finalizado com sucesso.';

} catch (Exception $e) {
	$date = date('d/m/Y H:i:s');
    echo '[' . $date . '] Exceção capturada: ',  $e->getMessage(), '\n';
}
