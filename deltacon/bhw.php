<?php
class bhw {

	public $client;
	public $uri;

	//Função para gerar a chave de acesso para as funções do WebService:
 	protected function accessKey($param) {

		$chaveinicial = 'DELTAWS';
		$chaveorigem = '';
		$chaveadicional = '';
		$chavefinal = '';

		for($i = 0; $i < strlen($chaveinicial); $i++){
			$chaveorigem .= ord($chaveinicial[$i]);
		}

		$keyParametro = array_keys($param)[0];

		if(strlen($param[$keyParametro]) > 7){
			$parametro = substr($param[$keyParametro], -7);
		}else{
			$parametro = $param[$keyParametro];
		}

		$parametro = (string) $parametro;

		for($i = 0; $i < strlen($parametro); $i++){
			$chaveadicional .= ord($parametro[$i]);
		}

		$chavefinal = $chaveadicional + $chaveorigem;

		return $chavefinal;

	}

	//Função para conectar com WebService e retornar os valores de cada função requerida:
	public function get($arg, $func) {

		$this->uri = 'http://peqnosdetalhes.dyndns.info:8080/RF2Server.asmx';
		$this->client = new SoapClient($this->uri.'?wsdl');

		$arg = array('chaveAcesso' => (string) $this->accessKey($arg))+$arg;
		$function = $func;
		$arguments = array($function => $arg);

		$options = array('location' => $this->uri);

		$retorno = $this->client->__soapCall($function, $arguments, $options);

		return $retorno;

	}

}
$soap = new bhw();
