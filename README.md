# Liberação da porta 8080.

### LocaWeb

```
sudo nmap -p 8080 -sTU pqnosdetalhes.com.br
```
__Retorno__

```
Starting Nmap 7.01 ( https://nmap.org ) at 2018-01-26 14:56 -02
Nmap scan report for pqnosdetalhes.com.br (179.188.50.50)
Host is up (0.31s latency).
rDNS record for 179.188.50.50: l70cnn0664.publiccloud.com.br
PORT     STATE         SERVICE
8080/tcp filtered      http-proxy
8080/udp open|filtered unknown

Nmap done: 1 IP address (1 host up) scanned in 7.06 seconds
```

### Servidor Local
```
sudo nmap -p 8080 -sTU peqnosdetalhes.dyndns.info
```
__Retorno__

```
Starting Nmap 7.01 ( https://nmap.org ) at 2018-01-26 15:00 -02
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.15 seconds
gustavo@windows-xp:~$ sudo nmap -p 8000 -sTU peqnosdetalhes.dyndns.info
```
